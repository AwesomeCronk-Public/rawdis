from requests import get, patch, post

botToken = None
botAppID = None
botPublicKey = None

apiVersion = 'v10'
baseURL = 'https://discord.com/api/{}/'.format(apiVersion)
requestHeaders = {
    'Authorization': 'Bot <httpAPI.setToken was not called>',
    'Content-Type': 'application/json; charset=UTF-8',
    'User-Agent': 'DiscordBot (https://gitlab.com/AwesobotAppIDCronk-Public/rawdis, 0.0.0)'
}


def setBotData(token: str, appID: str, publicKey: str):
    global botToken, botAppID, botPublicKey
    botToken = token
    botAppID = appID
    botPublicKey = publicKey

    requestHeaders['Authorization'] = 'Bot ' + token


def getCurrentApplication():
    return get(baseURL + 'applications/{}'.format(botAppID), headers=requestHeaders)

def editCurrentApplication(info: dict):
    return patch(baseURL + 'applications/{}'.format(botAppID), data=info, headers=requestHeaders)

def getGuildAuditLog(guildID):
    return get(baseURL + 'guilds/{}/audit-logs'.format(guildID), headers = requestHeaders)
