import time, traceback

from gatewayAPI import Gateway
from httpAPI import setToken as httpSetBotData


if __name__ == '__main__':
    with open('botData.json', 'r') as botDataFile:
        botDataJSON = json.load(botDataFile)
        token = botDataJSON['token']
        appID = botDataJSON['appID']
        publicKey = botDataJSON['publicKey']

    httpSetBotData(token, appID, publicKey)

    try:
        gateway = Gateway(token)
        gateway.connect()
        gateway.startHeartbeatLoop()
        gateway.startRecvLoop()
        gateway.identify(['GUILDS', 'GUILD_MESSAGES'])

        numLoops = 0

        while True:
            if numLoops % 5 == 0:
                print('loop', numLoops)

            event = gateway.getEvent()
            if event != None:
                print('Recved event:\n{}'.format(event))


            time.sleep(1)
            numLoops += 1

    except BaseException as e:
        print('\nCRASH')
        print(traceback.format_exc())

    finally:
        gateway.stopHeartbeatLoop()
        gateway.stopRecvLoop()
