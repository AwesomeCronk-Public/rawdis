import json, queue, random, sys, threading, time, traceback
from datetime import datetime, timedelta

from websockets.sync.client import connect as ws_connect
from websockets.exceptions import ConnectionClosed as WSConnectionClosed

from debug import debugHandler, _DebugHandler


# Insert a TRAFFIC method
def _debugTraffic(self: _DebugHandler, msg):
    self.message('TRAFFIC', msg)

_DebugHandler.traffic = _debugTraffic


class Gateway:
    # https://discord.com/developers/docs/topics/gateway
    # https://websockets.readthedocs.io/en/stable/reference/sync/client.html
    # https://discord.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-close-event-codes

    _opcodes = [
        'DISPATCH',
        'HEARTBEAT',
        'IDENTIFY',
        'PRESENCE_UPDATE',
        'VOICE_STATE_UPDATE',
        '<NOT_A_THING>',
        'RESUME',
        'RECONNECT',
        'REQUEST_GUILD_MEMBERS',
        'INVALID_SESSION',
        'HELLO',
        'HEARTBEAT_ACK'
    ]

    _intents = [
        "GUILDS",
        "GUILD_MEMBERS",
        "GUILD_MODERATION",
        "GUILD_EMOJIS_AND_STICKERS",
        "GUILD_INTEGRATIONS",
        "GUILD_WEBHOOKS",
        "GUILD_INVITES",
        "GUILD_VOICE_STATES",
        "GUILD_PRESENCES",
        "GUILD_MESSAGES",
        "GUILD_MESSAGE_REACTIONS",
        "GUILD_MESSAGE_TYPING",
        "DIRECT_MESSAGES",
        "DIRECT_MESSAGE_REACTIONS",
        "DIRECT_MESSAGE_TYPING",
        "MESSAGE_CONTENT",
        "GUILD_SCHEDULED_EVENTS",
        "<NOT_A_THING>",
        "<NOT_A_THING>",
        "<NOT_A_THING>",
        "AUTO_MODERATION_CONFIGURATION",
        "AUTO_MODERATION_EXECUTION",
        "<NOT_A_THING>",
        "<NOT_A_THING>",
        "GUILD_MESSAGE_POLLS",
        "DIRECT_MESSAGE_POLLS"
    ]   # All intents should be int of 53608447

    def __init__(
        self, token,
        heartbeatInterval=45, externalHeartbeat=None,
        maxQueueSize=256, putEventTimeout=15, getEventTimeout=1
    ):
        self.token = token

        self.heartbeatInterval = heartbeatInterval
        self._externalHeartbeat = externalHeartbeat
        self.continueHeartbeat = False
        # Old datetime values that should be older than the system time
        # TODO: Check that datetime.now() is at least later than this
        self.lastHeartbeat = datetime(2000, 1, 1)
        self.lastHeartbeatAck = datetime(2000, 1, 2)
        
        self.continueRecv = False
        self.lastSeqnum = None
        self.recvedEvents = queue.Queue(maxQueueSize)
        # putEventTimeout is longer by default to give more buffer against dropping events
        self.putEventTimeout = putEventTimeout
        self.getEventTimeout = getEventTimeout
        
        self.ws = None
        self.connected = False
        self.dbg = debugHandler('Gateway')


    def getOpcodeID(self, ident):
        if isinstance(ident, int):
            if -1 < ident < len(Gateway._opcodes):
                return ident
            else: return None

        elif isinstance(ident, str):
            properIdent = ident.upper()
            if properIdent in Gateway._opcodes:
                return Gateway._opcodes.index(properIdent)
            else: return None

        else:
            raise TypeError('Expected int or str, not {}'.format(type(ident)))

    def getOpcodeName(self, ident):
        if isinstance(ident, str):
            if ident in Gateway._opcodes:
                return ident
            else: return None

        elif isinstance(ident, int):
            if -1 < ident < len(Gateway._opcodes):
                name = Gateway._opcodes[ident]
                if name != '<NOT_A_THING>': return name
                else: return None
            else: return None

        else:
            raise TypeError('Expected str or int, not {}'.format(type(ident)))

    def getIntentsInt(self, intents):
        if intents == ['<ALL>']:
            intents = []
            for intent in Gateway._intents:
                if intent != '<NOT_A_THING>':
                    intents.append(intent)

        intentsInt = 0
        for intent in intents:
            intent = intent.upper()
            if intent in Gateway._intents:
                intentsInt += 2 ** Gateway._intents.index(intent)
            else:
                self.dbg.error('Intent {} not recognized'.format(intent))
                sys.exit(1)

        return intentsInt


    class Event:
        def __init__(self, opcode, data, seqnum, eventname):
            self.opcode = Gateway.getOpcodeID(None, opcode)
            self.data = data
            self.seqnum = seqnum
            self.eventname = eventname
            self.dbg = debugHandler('Gateway.Event')

            self.opcodeName = Gateway.getOpcodeName(None, opcode)
            if self.opcodeName == None: self.opcodeName = '???'

        def fromJSON(jsonRaw):
            jsonData = json.loads(jsonRaw)
            try:
                return Gateway.Event(jsonData['op'], jsonData['d'], jsonData['s'], jsonData['t'])
            except KeyError as err:
                dbg = debugHandler('Gateway.Event.fromJSON')
                dbg.error('Missing expected value ({}):\n{}'.format(', '.join(err.args), jsonRaw))
                sys.exit(1)

        def toJSON(self, keepEmpties=False):
            jsonDict = {}
            if self.opcode    != None or keepEmpties: jsonDict['op'] = self.opcode
            if self.data      != None or keepEmpties: jsonDict['d']  = self.data
            if self.seqnum    != None or keepEmpties: jsonDict['s']  = self.seqnum
            if self.eventname != None or keepEmpties: jsonDict['t']  = self.eventname
            return json.dumps(jsonDict)

        def __repr__(self):
            return 'Gateway.Event\n- opcode: {} ({})\n- data: {}\n- seqnum: {}\n- eventname: {}'.format(self.opcode, self.opcodeName, self.data, self.seqnum, self.eventname)


    # Send an event to the gateway API
    def _sendEvent(self, event):
        self.dbg.traffic('Send:\n{}'.format(event))

        jsonData = event.toJSON()
        # self.dbg.debug(jsonData)
        self.ws.send(jsonData)

        # TODO: Handle ConnectionClosed

    # Recv an event from the gateway API and, depending on the event and provided flags, put it in
    # self.recvedEvents. If a timeout occurs or bullshit data is recved, return None, else return
    # the event, indicating the event was handled internally or put in self.recvedEvents.
    def _recvEvent(self, handleInternalEvents=True, passInternalEvents=False):
        try: rawEvent = self.ws.recv(timeout=10)
        
        # Handle timeout
        except TimeoutError:
            return None

        # Handle Discord closing the connection
        except WSConnectionClosed as err:
            self.dbg.warning('Discord closed the connection ({}: {})'.format(err.rcvd.code, err.rcvd.reason))
            return None
            # TODO: Reconnect

        try: event = Gateway.Event.fromJSON(rawEvent)   # TODO: Handle errors from Event

        # Handle bad packet data
        except json.decoder.JSONDecodeError:
            self.dbg.warning('Discord sent packet that wasn\'t JSON:\n{}'.format(repr(rawEvent)))
            return None

        self.dbg.traffic('Recv:\n{}'.format(event))

        # Handle internal events
        if handleInternalEvents:
            if event.opcode == self.getOpcodeID('HEARTBEAT_ACK'):
                self.lastHeartbeatAck = datetime.now()
                
            elif event.opcode == self.getOpcodeID('RECONNECT'):
                self.dbg.warning('Discord sent reconnect instruction')

        # HELLO and READY events are not actually handled here; they are only treated as internal
        # to ensure that they are returned and not put in self.recvedEvents incorrectly

        # If acceptable, put the event in self.recvedEvents
        if (not event.opcodeName in ('HELLO', 'READY', 'HEARTBEAT_ACK', 'RECONNECT')) or passInternalEvents:
            try:
                self.recvedEvents.put(event, timeout=self.putEventTimeout)

            except queue.Empty:
                dbg.warning('{}.recvedEvents failed to put event within {}s, dropping event:\n{}'.format(self, self.putEventTimeout, event))

            else:
                if event.seqnum != None: self.lastSeqnum = event.seqnum

        else:
            if event.seqnum != None: self.lastSeqnum = event.seqnum

        return event


    def _heartbeatLoop(self):
        # Loop every 250ms while self.continueHeartbeat, check if time for heartbeat and if so, send it
        try:
            dbg = debugHandler('Gateway._heartbeatLoop')
            dbg.debug('Entered try')
            heartbeatTimedelta = timedelta(seconds=self.heartbeatInterval)

            # Jitter the first heartbeat as per Discord docs
            initialDelay = random.uniform(0.0, self.heartbeatInterval)
            dbg.debug('Delaying first heartbeat by {}s'.format(initialDelay))
            time.sleep(initialDelay)

            while self.continueHeartbeat:
                now = datetime.now()
                if now >= self.lastHeartbeat + heartbeatTimedelta:

                    if self.lastHeartbeatAck < self.lastHeartbeat:
                        dbg.warning('Discord did not acknowledge last heartbeat')   # TODO: Trigger a reconnect

                    self.lastHeartbeat = now

                    heartbeatEvent = Gateway.Event('HEARTBEAT', self.lastSeqnum, None, None)
                    self._sendEvent(heartbeatEvent)
                    dbg.debug('Heartbeat')

                    # heartbeatAckEvent = self.recvEvent()
                    # if heartbeatAckEvent.opcode != self.getOpcodeID('HEARTBEAT_ACK'):
                    #     dbg.error('Discord did not acknowledge the heartbeat\n{}'.format(heartbeatAckEvent))
                    #     sys.exit(1)

                    if self._externalHeartbeat != None:
                        self._externalHeartbeat()

                time.sleep(0.25)
        finally:
            dbg.debug('Exiting via finally')

    def startHeartbeatLoop(self):
        self.dbg.debug('Starting heartbeat loop...')
        self.continueHeartbeat = True
        self.heartbeatThread = threading.Thread(target=self._heartbeatLoop)
        self.heartbeatThread.start()
        self.dbg.debug('Heartbeat loop started')

    def stopHeartbeatLoop(self):
        if self.continueHeartbeat:
            self.dbg.debug('Stopping heartbeat loop...')
            self.continueHeartbeat = False
            self.heartbeatThread.join()
            self.dbg.debug('Heartbeat loop stopped')
        else:
            self.dbg.debug('Heatbeat loop not running')


    def _recvLoop(self):
        try:
            dbg = debugHandler('Gateway._recvLoop')
            dbg.debug('Entered try')

            while self.continueRecv:
                try: self._recvEvent()
                except: self.dbg.warning('Error while receiving event:\n{}'.format(traceback.format_exc()))

                time.sleep(0.25)
        finally:
            dbg.debug('Exiting via finally')

    def startRecvLoop(self):
        self.dbg.debug('Starting recv loop...')
        self.continueRecv = True
        self.recvThread = threading.Thread(target = self._recvLoop)
        self.recvThread.start()
        self.dbg.debug('Recv loop started')

    def stopRecvLoop(self):
        if self.continueRecv:
            self.dbg.debug('Stopping recv loop...')
            self.continueRecv = False
            self.recvThread.join()
            self.dbg.debug('Recv loop stoppped')
        else:
            self.dbg.debug('Recv loop not running')


    def connect(self, url='wss://gateway.discord.gg/?v=10&encoding=json'):
        self.dbg.debug('Connecting to Discord...')
        self.ws = ws_connect(url, open_timeout=10, close_timeout=20)
        self.dbg.traffic('Connected to {}'.format(url))

        # TODO: What if Discord takes longer than the timeout period to respond?
        helloEvent = self._recvEvent()
        if helloEvent.opcode != self.getOpcodeID('HELLO'):
            self.dbg.error('Discord returned non-hello event on connection\n{}'.format(repr(helloEvent)))
            sys.exit(1)

        try:
            discordHeartbeatInterval = helloEvent.data['heartbeat_interval'] / 1000     # API uses ms, I use s
            if discordHeartbeatInterval < self.heartbeatInterval:
                self.dbg.info('Discord specified a shorter heartbeat interval ({}s), ignoring supplied value of {}s'.format(discordHeartbeatInterval, self.heartbeatInterval))
                self.heartbeatInterval = discordHeartbeatInterval
        except KeyError:
            self.dbg.warning('Discord did not specify a heartbeat interval, moving on')
        
        self.connected = True
        self.dbg.debug('Connected')

    # TODO: Implement disconnect
    # TODO: Implement reconnect

    def getEvent(self):
        if self.recvedEvents.empty():
            return None

        # Offer a timeout of 1 second just in case the recv loop is putting an event at this moment
        try:
            event = self.recvedEvents.get(timeout=self.getEventTimeout)

        except queue.Empty:
            self.dbg.warning('{}.recvedEvents failed to get an event within {}s'.format(self, self.getEventTimeout))
            return None

        else:
            return event


    def identify(self, intents):
        intentsInt = self.getIntentsInt(intents)

        data = {
            'token': self.token,
            'intents': intentsInt,
            'properties': {
                'os': 'linux',  # TODO?: OS detection
                'browser': 'rawdis',
                'device': 'rawdis'
            }
        }

        event = Gateway.Event('IDENTIFY', data, None, None)

        self._sendEvent(event)

        # TODO: receive ready event

